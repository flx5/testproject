# Must be run privileged

FROM debian:buster

RUN apt-get update
RUN apt-get install -y vagrant vagrant-libvirt libvirt-daemon-system rsync sudo

# Just for faster debugging
RUN vagrant box add debian/buster64 --provider libvirt

COPY Vagrantfile .

CMD service libvirtd start && service virtlogd start && vagrant up

#RUN vagrant plugin install vagrant-libvirt
